<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Parser
use Nathanmac\Utilities\Parser\Parser;

class TestController extends Controller
{

    
    public function test()
    {

        $payload = '<list xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../xsd/recipes.xsd">
        <item id="1" recipeId="1666" name="mk_wooden_arrow" craftLevel="1" type="dwarven" successRate="100">
            <ingredient id="1864" count="4" />
            <ingredient id="1869" count="2" />
            <production id="17" count="500" />
            <statUse name="MP" value="30" />
        </item>
        <item id="3" recipeId="1787" name="mk_willow_staff" craftLevel="1" type="dwarven" successRate="100">
            <ingredient id="2006" count="1" />
            <ingredient id="1869" count="12" />
            <ingredient id="1872" count="24" />
            <ingredient id="1864" count="12" />
            <production id="8" count="1" />
            <statUse name="MP" value="30" />
	    </item>        
        </list>';

        $parser = new Parser();
        $parsedFile = $parser->xml($payload);

        // $Counter = 0;
        // foreach ($parsedFile['item'] as $XMLItem):
        //   if (isset($XMLItem['@type'])):
        //     $Counter++;
        //   endif;
        // endforeach;
        // if ($Counter == 0):
        //   foreach ($parsedFile as $XMLItem):
        //     if (isset($XMLItem['@type'])):
        //       $Counter++;
        //     endif;
        //   endforeach;
        // endif;

        $texte = count($parsedFile['item'][0]['ingredient']);


        return dd($parsedFile);


        }

}
