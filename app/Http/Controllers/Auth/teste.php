static function ProcessXML($request) {
    // Verify if the call is with Ajax method
    if($request->Ajax()):
      // Storage::disk('local')->put('file.txt', 'Contents');
      // $Items = (object) [
      //   'name' => $_FILES['files']['name'],
      //   'size' => $_FILES['files']['size'],
      //   'status' => 'success',
      //   // 'url' => '#',
      //   // 'thumbnailUrl' => '#',
      //   // 'deleteUrl' => '#',
      //   // 'deleteType' => 'DELETE'
      // ];
      //
      //
      // return response()->json([
      //                         'type' => 'Success',
      //                         'msg' => 'Item added into database',
      //                         '$_FILES' => $_FILES,
      //                         // This is for jquery-file-upload required response
      //                         'files' => [
      //                           $Items
      //                         ],
      //                         'trueXML' => file_get_contents($_FILES['files']['tmp_name'][0]),
      //                         'parsedXML' => simplexml_load_file($_FILES['files']['tmp_name'][0]),
      //                         'tmpName' => $_FILES['files']['tmp_name'][0]
      //                         // 'fileData' => Storage::disk('local')->get('file.txt')
      //                     ]);
      $IP = $request->ip();
      // XML Status
      $Count = Sunrise::ServerXML('count', $IP);
      $Limit = Sunrise::ServerXML('limit', $IP);
      $Remaining = $Limit - $Count;
      if ($Count < $Limit):
      // Set the Characters table
      $Table = "characters";
      if (!isset($_FILES['files']['name'])):
        $XMLInput = $request->input('xml');
      else:
        $XMLInput = file_get_contents($_FILES['files']['tmp_name'][0]);
      endif;
      // Get the character old nick
      $CustomTable = $request->input('custom_table');
      $parser = new Parser();
      $parsedFile = $parser->xml($XMLInput);
      $Counter = 0;
      foreach ($parsedFile['item'] as $XMLItem):
        if (isset($XMLItem['@type'])):
          $Counter++;
        endif;
      endforeach;
      if ($Counter == 0):
        foreach ($parsedFile as $XMLItem):
          if (isset($XMLItem['@type'])):
            $Counter++;
          endif;
        endforeach;
      endif;
      if ($Counter > $Remaining):
        $Items = (object) [
          'name' => $_FILES['files']['name'],
          'size' => $_FILES['files']['size'],
          'error' => 'Reached the limit of upload items from xml to database || Limit Remaining: '.$Remaining.' - Items in this file: '.$Counter,
          'count' => Sunrise::ServerXML('count'),
          'limit' => Sunrise::ServerXML('limit'),
          'percent' => Sunrise::ServerXMLStatistic(true).'%'
        ];
        return response()->json([
                                'files' => [
                                  $Items
                                ]
                            ]);
      endif;
      if ($Counter > 1):
        foreach ($parsedFile['item'] as $parsedXML):
          try {
            $XMLToDatabase = Self::SaveCustomXMLInDB($parsedXML, $CustomTable); // Custom Itens
          } catch (\Exception $e) {
            $Items = (object) [
              'name' => $_FILES['files']['name'],
              'size' => $_FILES['files']['size'],
              'status' => 'error',
              'error' => 'Any item in XML broke the proccess'
            ];
            return response()->json([
              'exception' => $e,
              'files' => [
                $Items
              ]
            ]);
          }
        endforeach;
        if ($XMLToDatabase == true):
          Sunrise::UpdateServerXML($Counter);
        endif;
        $Items = (object) [
          'name' => $_FILES['files']['name'],
          'size' => $_FILES['files']['size'],
          'status' => 'success',
          'msg' => 'All - '.$Counter.' - Items was added into database',
          'count' => Sunrise::ServerXML('count'),
          'limit' => Sunrise::ServerXML('limit'),
          'percent' => Sunrise::ServerXMLStatistic(true).'%'
        ];
        return response()->json([
                                'files' => [
                                  $Items
                                ]
                            ]);
      else:
        try {
          $XMLToDatabase = Self::SaveCustomXMLInDB($parsedFile['item'], $CustomTable);
          if ($XMLToDatabase == true):
            Sunrise::UpdateServerXML($Counter);
          endif;
          $Items = (object) [
            'name' => $_FILES['files']['name'],
            'size' => $_FILES['files']['size'],
            'status' => 'success',
            'msg' => 'Item added into database',
            'count' => Sunrise::ServerXML('count'),
            'limit' => Sunrise::ServerXML('limit'),
            'percent' => Sunrise::ServerXMLStatistic(true).'%'
          ];
          return response()->json([
                                  // This is for jquery-file-upload required response
                                  'files' => [
                                    $Items
                                  ]
                              ]);
        } catch (\Exception $e) {
          $Items = (object) [
            'name' => $_FILES['files']['name'],
            'size' => $_FILES['files']['size'],
            'status' => 'error',
            'error' => 'Item in XML have a incorrect structure and broke the proccess',
            'count' => Sunrise::ServerXML('count'),
            'limit' => Sunrise::ServerXML('limit'),
            'percent' => Sunrise::ServerXMLStatistic(true).'%'
          ];
          return response()->json([
                                  'exception' => $e,
                                  'files' => [
                                    $Items
                                  ],
                              ]);
        }
      endif;
      elseif ($Count >= $Limit):
        $Items = (object) [
          'name' => $_FILES['files']['name'],
          'size' => $_FILES['files']['size'],
          'error' => 'Reached the limit of upload items from xml to database',
          'count' => Sunrise::ServerXML('count'),
          'limit' => Sunrise::ServerXML('limit'),
          'percent' => Sunrise::ServerXMLStatistic(true).'%'
        ];
        return response()->json([
                                'files' => [
                                  $Items
                                ]
                            ]);
      endif;
   endif; // Ajax
  }